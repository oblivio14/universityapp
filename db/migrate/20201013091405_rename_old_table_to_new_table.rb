class RenameOldTableToNewTable < ActiveRecord::Migration[5.2]
  def change
    rename_table :student_courses_tables, :student_courses
  end
end
